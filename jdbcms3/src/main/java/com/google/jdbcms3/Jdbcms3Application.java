package com.google.jdbcms3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jdbcms3Application {

	public static void main(String[] args) {
		SpringApplication.run(Jdbcms3Application.class, args);
	}

}
